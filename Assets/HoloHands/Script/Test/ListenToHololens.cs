﻿using HoloToolkit.Unity.InputModule;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.WSA.Input;

public abstract class ListenToHololens : MonoBehaviour, IInputClickHandler, IInputHandler, IFocusable
{
    void Start(){
        InputManager.Instance.AddGlobalListener(this.gameObject);
    }
    public abstract void OnInputClicked(InputClickedEventData eventData);
    public abstract void OnInputDown(InputEventData eventData);
    public abstract void OnInputUp(InputEventData eventData);
    public abstract void OnFocusEnter();
    public abstract void OnFocusExit();
    //ISourceStateHandler
    //IManipulationHandler
    //IHoldHandler
    //INavigationHandler
    //IControllerInputHandler
    //IBoundingBoxStateHandler
    //ISpeechHandler
    //IDictationHandler
}